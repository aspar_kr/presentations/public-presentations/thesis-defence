---
title: "The how and why of being a FAIR scientist"
subtitle: "or how learned to stop worrying and love [excel]{.alert} for data entry"
author:
  - name: "[*Sibbe Bakker*](https://orcid.org/0009-0001-0696-1684)"
    affiliation: Genetics
  - name: "Mariana Silva Coutos"
    affiliation: Genetics
  - name: "Anna Fensel"
    affiliation: AI & Data science
format: 
    revealjs:
        csl: diabetologia.csl
        theme: aspar.scss
        reference-location: document
        slide-number: c/t
        width: 1600
        height: 900
        # logo: "https://git.wur.nl/aspar_kr/aspar/-/raw/main/logo.png?ref_type=heads&inline=false"
        standalone: true
        embed-resources: true
        navigation-mode: vertical
        controls-layout: bottom-right
title-slide-attributes:
    data-background-size: cover
    data-background-image: inclusions/blurred-asper.jpg
    data-background-opacity: "0.5"
date: "2024-01-18"
bibliography: "BibDataBase.bib"
---




```{r message=FALSE, warning=TRUE, include=FALSE}
library(plantuml)
```

# How and Why of data sharing {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}

<!-- WHY care so much 


what my work is is not clear.

do I have permission from all parties?

Don't show _how_ it works, show _what_ it does!

* Your research is important and complex, but our research data is too simple
  to understand.
* Usually, we cannot validate the errors.
* Basic things, like dates are unknown.
-->


::: {.r-stack}

::: {.fragment .fade-out}
> Have you ever been confused about data?
:::


::: {.fragment .fade-in-then-out}
![Maybe you cannot access it?](inclusions/data_statement_goncalvesAzoleResistantAspergillusFumigatus2021.png){width="1500" height="auto"}
:::


::: {.fragment .fade-in-then-out}
![How can you plot this?](inclusions/BF-sheet.png){width="1600" height="auto"}
:::


::: {.fragment .fade-in-then-out}
![Are we missing data?](inclusions/BF-missing-data.png){width="1600" height="auto"}
:::


::: {.fragment .fade-in-then-out}
![Do the colours mean anything?](inclusions/what-do-columns-mean.png){width="1600" height="auto"}
:::

::: {.fragment .fade-in-then-out}
![What unit concentrations was measured? What coordinate system are we using?](inclusions/data_statement_caoPrevalenceAzoleResistantAspergillus2021.png){width="1400" height="auto"}
:::


::: {.fragment .fade-in-then-out}
![Some questions still](inclusions/what-soil-biosample.png){width="1400" height="auto"}
<!-- Questions
* How is aspergillus a strain?
* What soil type
-->

:::

:::



<!-- Its not only not fair, but this has consequences on the life of people -->

## What do we find? {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}

::: {.r-stack}

::: {.fragment fragment-index=1 .fade-in-then-out}
![](inclusions/difficult_ramsey.gif){width="1400" height="auto"}
::: 

::: {.fragment fragment-index=2 .fade-in}
> Most research data is $\dots$

* Fickle;

* Impossible to validate;

* In the best case, only well described for humans.
:::

:::

## Consequences? {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}

::: incremental

* Delayed research;

* Critical errors in data analysis [@abeysooriyaGeneNameErrors2021];

* Increased barrier to entry for labs that lack resources;

* Impossible questions (missing metadata);
<!-- Environmental descriptions -->

* For important data this may [directly]{.altert} affect peoples lives.

![](inclusions/real_life.png){.fragment width="900" height="auto" .center}

:::

# What can make your work more FAIR? {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}

:::  {.r-fit-text}

> The FAIRDS

:::

::: columns

::: {.column width="50%"}

::: incremental

* made by Nijsen _et al._ @nijsseFAIRDataStation2023 (SSB)

* Validates excel sheets against ENA standards.

* Converts them to a [stable]{.alert} and [scalable]{.alert} format.

:::

:::

::: {.column width="50%"}

::: {layout-nrow=1}

![Jasper Koehorst](inclusions/jasper_koehorst.png){width="170" height="170"}

![Bart Nijsen^[Does not want to share his likeness]](inclusions/bart.png){width="170" height="170"}

![Peter J Schaap](inclusions/PJ_schaap.jpeg){width="170" height="170"}

:::



:::

:::

## FAIR enough, but what does it do? {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}


::: {.r-stack}

::: {.fragment}

> Going from excel $\rightarrow$ `rdf`.

:::

![](inclusions/good-rdf.png){.fragment .fade-in-then-out width="1000" height="80%"}

![](inclusions/airsample-graph.png){.fragment .fade-in width="1000" height="80%"}
:::

## How does it work? {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}


::: {.notes}
* Classes available within the `ASPAR_KR` database. Each class `owns' lower level classes. For example, a sample has associated assays.
* Each class is a sheet in an excel file.
* Mention that the programme is written in `java` <br>
Meaning that most programmers can contribute to it.
* Mention that I have permission to adapt a branch of the software.

:::

```{plantuml main-classes, eval = TRUE, echo = FALSE, plantuml.format = "png", plantuml.path = "graphs", plantuml.preview = TRUE, out.width = '100%'}
@startuml
skinparam monochrome reverse
left to right direction
Investigation *-- Study
Study *-- ObservationUnit
ObservationUnit *-- Sample
Sample *-- Assay

Sample *-- Sample : Derives
@enduml
```

::: {.r-stack}

::: {.fragment fragment-index=1 .fade-in-then-out}

::: columns

::: {.column width="50%}

* Experimental design is part of the dataset.

* Minimum information standards are used.

* New templates can be [introduced](https://bookdown.org/sibbe_l_bakker/aspar_kr/appendix.html#own-packages).
:::

::: {.column width="50%}

* You make `excel` templates.
* Fill these in with your data.
* Upload them, `FAIRDS` makes RDF.

:::

:::

:::

::: {.fragment fragment-index=2 .fade-in-then-out}

**Investigation** class

* Title of the Investigation
* Study authors.
* Publication details.
* Abstract.

:::

::: {.fragment fragment-index=3 .fade-in-then-out}

**Study** class, sub question of Investigation.

* Title of the study.
* Aim and description.

:::

::: {.fragment fragment-index=4 .fade-in-then-out}

**Observation** class, what a study observed.

* Name and description
* Observation level factors.

:::

::: {.fragment fragment-index=5 .fade-in-then-out}

**Sample** class, what a study observed.

* Name and description
* Sample level factors.
* Sample specific metadata

:::

::: {.fragment fragment-index=6 .fade-in-then-out}

**Assay** class, what was measured.

* Name and description
* Assay specific metadata

:::


:::


## An usecase -- verifying data  {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}

> Hylke's air sampling data

```sparql
PREFIX schema: <http://schema.org/>
prefix geo: <http://www.opengis.net/ont/geosparql#> 
prefix sf: <http://www.opengis.net/ont/sf#> 
PREFIX cats: <http://cats.org/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>
PREFIX fair: <http://fairbydesign.nl/ontology/>
prefix jerm:     <http://jermontology.org/ontology/JERMOntology#> 
SELECT *
WHERE {
    
    # Get properties of the DeltaTrap.
    ?deltaTrap fair:packageName 'DeltaTrap' ;
        <https://w3id.org/mixs/terms/0000011> ?arrival_date ;
         geo:hasGeometry/geo:asWKT ?point .

    ?observationalUnit jerm:hasPart ?deltaTrap ;
                schema:identifier ?obsId .


    # Get properties of the culture
    ?deltaTrap fair:derives ?twoLayerCulture ;
        fair:start_date ?start_date ;
        fair:end_date ?end_date ;
        <https://w3id.org/mixs/terms/0000011> ?analysis_date ;
    
    # What is the amount of time the seals were exposed?
    BIND(day(?end_date - ?start_date) as ?air_exposure_days)
    # What is the transfer time?
    BIND(day(?arrival_date - ?end_date) as ?transfer_time)
    # Time untill analysis after arrival?
    BIND(day(?analysis_date - ?arrival_date) as ?time_to_analysis)

    # Distance from WUR
    SERVICE <https://query.wikidata.org/sparql> {
        # What things are a municipality?
        ?municipality wdt:P31 wd:Q2039348.
        # What things have a place?
        ?municipality wdt:P625 ?placeOfInterest .
        # Take only the thing that is near the place of interest.
        FILTER(?municipality = wd:Q1305) . # Arnhem
    }
    BIND (geof:distance(?point, ?placeOfInterest, uom:kilometre) as ?d_km)
}
```


## The results {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}

::: {#fig-elephants layout-ncol=2}

![Transfer time to the Lab](inclusions/transfer-time.png)


![Relation time and distance](inclusions/hylke-regress.png)

Relation between distance and time in the postal system.
:::

# Outlook  {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}

> What lays ahead?

::: incremental

* Get started today!

* Every bit helps.

:::

## I remind you: it is possible {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}

![](inclusions/perfect.png){width="1400" height="auto"}

::: aside

Even this record is not perfect, but lets leave that for the discussion.

:::


## Storing the data {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}

::: columns

::: {.column width="30%"}

::: incremental

* Departmental databases?

* One central repository?
    
    * in ENA?
    
    * What about phenotype data?

* Useages?

:::

:::

::: {.column width="70%" .nostretch}

![How a database system can be established, redrawn from @costelloStrategiesSustainabilityOnline2014](inclusions/how-a-database-is-established.png){width="100%"}
:::

::: 

## Usage of FAIR data to answer questions {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}

> Besides _just_ [transparancy]{.alert} FAIR data can also be used for $\dots$

::: incremental

* How is the genotype related to environmental factors? <br>
  Is there a relation between the climate and genome data?


:::

# Conclusion {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}

::: incremental


* Researchers need to be mindful of their data.

::: {.fragment}
>  Don't be afraid to make 'FAIR data' mistakes because you lack knowledge;
just get started and ask people like me.
:::


:::


# Additional slides

## Challenges

### Personal

* Interviewing people.

* Contributing to the `java` programme.

### Domain

* Running all of the software correctly

* Understanding what people are doing in the lab.



# References {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}

## More information {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}


::: columns

::: {.column width="50%"}

* [`sibbe.bakker@wur.nl`](mailto:sibbe.bakker@wur.nl)

:::

::: {.column width="50%"}

* Read my [thesis](https://git.wur.nl/aspar_kr/presentations/public-presentations/thesis-defence/-/raw/main/inclusions/aspar_kr_thesis-sibbe.pdf?ref_type=heads&inline=false).

:::

:::

My thesis could not be made without the help of a lot of collaborators:

> [Jasper]{.alert} for offering his help with the `FAIRDS`.
[Anna]{.alert} and [Mariana]{.alert}, for support and guidance during the
thesis; [Martin]{.alert} and [Hylke]{.alert}, for their data and aiding with 
the usecases; [Murambiwa]{.alert}, [Christopher]{.alert} for their data and
critical discussion; and  [Sijmen]{.alert} for the insight on the structure of
my thesis.

::: aside

This presentation was made with `quarto`. Contributors to opensource software 
usually go unmentioned, but it's vital to keep in mind that software comes from
**people**: [Aaron Swartz]{.alert} (`Web documents` and the FOSS movement),
[Ross Ihaka]{.alert}, [Robert Gentleman]{.alert} (`R`) and
[Leslie Lamport]{.alert} ($\LaTeX$).

:::

## Cited works {background-color="black" background-image="inclusions/blurred-asper.jpg" background-size="cover" background-opacity="0.5"}

::: {#refs}
:::
